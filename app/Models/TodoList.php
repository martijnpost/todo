<?php

namespace App\Models;

use App\Models\User;
use App\Models\Todo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TodoList extends Model
{
    use softDeletes;

    protected $fillable = ['title', 'description'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function todos()
    {
        return $this->hasMany(Todo::class);
    }

    public function scopeSearch(Builder $query, String $search)
    {
        return $query->where(function($query) use ($search) {
            $query->where('title', 'like', '%' .$search. '%')
                ->orWhere('description', 'like', '%' .$search. '%');
        });
    }

    public function scopeMyLists(Builder $query)
    {
        return $query->where('user_id', auth()->user()->id);
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function(TodoList $todoList) {
           $todoList->todos()->delete();
        });
    }
}
