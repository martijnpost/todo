<?php

namespace App\Models;

use App\Models\Tag;
use App\Models\TodoList;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Todo extends Model
{
    use softDeletes;

    protected $fillable = ['title', 'description', 'done', 'file', 'deadline', 'url'];

    public function todoList()
    {
        return $this->belongsTo(TodoList::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }

    public function toggleStatus()
    {
        $this->done = !$this->done;

        return $this;
    }
}
