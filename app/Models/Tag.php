<?php

namespace App\Models;

use App\Models\Todo;
use App\Scopes\SortAlphabeticallyScope;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['name'];

    public function todos()
    {
        return $this->belongsToMany(Todo::class)->withTimestamps();
    }

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SortAlphabeticallyScope);
    }
}
