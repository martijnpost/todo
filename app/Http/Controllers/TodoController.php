<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTodo;
use App\Models\Tag;
use App\Models\Todo;
use App\Models\TodoList;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TodoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TodoList $todoList, Request $request)
    {
        $todoList = TodoList::withCount(['todos', 'todos as todos_done_count' => function ($query) {
            $query->where('done', 1);
        }])->where('id', $todoList->id)->first();

        $todos = Todo::where('todo_list_id', $todoList->id);

        if(isset($request->tagFilter)) {
            $todos = $todos->whereHas('tags', function (Builder $q) use ($request) {
                $q->where('tags.id', $request->tagFilter);
            });
        }

        $todos = $todos->orderBy('done')->orderBy('deadline')->get();

        $tags = Tag::whereHas('todos', function($query) use ($todoList) {
            $query->where('todo_list_id', $todoList->id);
        })->distinct()->get();

        // status progress bar
        $status = $todoList->todos_count != 0 ? intval($todoList->todos_done_count/$todoList->todos_count*100) : null;

        return view('todos.index', compact('todoList', 'status', 'todos', 'tags'))->with('tagFilter', $request->tagFilter);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(TodoList $todoList)
    {
        $this->authorize('todo.create', $todoList);

        $tags = Tag::all();

        return view('todos.create', compact('todoList', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTodo $request, TodoList $todoList)
    {
        $request->validated();

        // store file
        if($request->hasFile('file')){
            $file = $request->file('file');
            $path = $file->store('files','public');
        }

        $todo = Todo::make(array_merge($request->validated(), [
            'file' => $path ?? null
        ]));

        $todo->todoList()->associate($todoList)->save();

        // attach tags
        $tags = $request->input('tags');
        $todo->tags()->attach($tags);

        $request->session()->flash('status', 'Todo created');

        return redirect()->route('todo-lists.todos.index', $todoList);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo)
    {
        return view('todos.show', compact('todo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Todo $todo)
    {
        $this->authorize('todo.update', $todo);

        $tags = Tag::all();

        return view('todos.edit', compact('todo', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTodo $request, Todo $todo)
    {
        $validatedData = $request->validated();

        if($request->hasFile('file')){
            $file = $request->file('file');
            $path = $file->store('files','public');
        }

        // attach tags
        $tags = $request->input('tags');
        $todo->tags()->sync($tags);

        $todo->update(array_merge($validatedData, [
            'file' => $path ?? null
        ]));

        $request->session()->flash('status', 'Todo updated');

        return redirect()->route('todo-lists.show', [$todo->todoList]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Todo $todo
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Request $request, Todo $todo)
    {
        $this->authorize('todo.delete', $todo);

        Storage::delete($todo->file);

        $todo->delete();

        $request->session()->flash('status', 'Todo deleted');

        return redirect()->back();
    }

    public function status(Request $request, Todo $todo)
    {
        $this->authorize('todo.update', $todo);

        $todo->toggleStatus()->save();

        $message = "Status '$todo->title' changed to: ".($todo->done ? 'Done' : "Todo");

        $request->session()->flash('status', $message);

        return redirect()->back();
    }

    public function delay(Request $request, Todo $todo)
    {
        $this->authorize('todo.update', $todo);

        $deadline = new Carbon($todo->deadline);

        $todo->deadline = $deadline->addDays((int)$request->delay);
        $todo->save();

        $request->session()->flash('status', "$request->delay days added to '$todo->title'");

        return redirect()->route('todo-lists.todos.index', $todo->todoList);
    }
}
