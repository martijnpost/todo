<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       $users = User::with(['todoLists' => function ($query) {
           $query->withCount('todos')->orderBy('title');
       }])->orderBy('name')->paginate(7);

        return view('home', compact('users'));
    }
}
