<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTodoList;
use App\Models\TodoList;
use Illuminate\Http\Request;

class TodoListsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->input('search') ?? '';

        $todoLists = TodoList::withCount(['todos', 'todos as todos_done_count' => function ($query) {
            $query->where('done', 1);
        }])->myLists()->search($search)->get();

        return view('todolists.index', compact('todoLists', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('todolists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTodoList $request)
    {
        $validatedData = $request->validated();

        $todoList = TodoList::make($validatedData);

        $todoList->user()->associate(auth()->user())->save();

//        TodoList::create(array_merge($validatedData, [
//            'user_id' => auth()->user()->id
//        ]));

        return redirect()->route('todo-lists.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(TodoList $todoList)
    {
        return redirect()->route('todo-lists.todos.index', $todoList);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(TodoList $todoList)
    {
        $this->authorize('todoList.update', $todoList);

        return view('todolists.edit', compact('todoList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTodoList $request, TodoList $todoList)
    {
        $validatedData = $request->validated();

        $todoList->update($validatedData);

        return redirect()->route('todo-lists.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, TodoList $todoList)
    {
        $this->authorize('todoList.delete', $todoList);

        $todoList->delete($todoList);

        $request->session()->flash('status', 'Todo List deleted!');

        return redirect()->route('todo-lists.index');
    }
}
