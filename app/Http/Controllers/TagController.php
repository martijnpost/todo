<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTag;
use App\Models\Tag;

class TagController extends Controller
{
    public function index()
    {
       $tags = Tag::withCount('todos')->paginate(10);

        return view('tags.index', compact('tags'));
    }

    public function store(StoreTag $request)
    {
        $validatedData = $request->validated();

        $request->session()->flash('status', "Tag '$request->name' created");

        Tag::create($validatedData);

        return redirect()->back();
    }
}
