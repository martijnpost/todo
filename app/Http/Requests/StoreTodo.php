<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTodo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'required', 'min:3', 'max:50'
            ],
            'description' => [
                'max:2000'
            ],
            'tags' => [
                'required', 'array', 'between:1,3'
            ],
            'file' => [
                'file', 'max:2048'
            ],
            'url' => [
                'nullable', 'url'
            ],
            'deadline' => [
                'nullable', 'date', 'after:today'
            ],
        ];
    }
}
