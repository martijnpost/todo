<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Todo;
use Faker\Generator as Faker;

$factory->define(Todo::class, function (Faker $faker) {
    return [
        'title' => $faker->words(3, true),
        'description' => $faker->text(),
        'deadline' => now()->addDays($faker->randomDigitNotNull),
        'url' => $faker->url
    ];
});

$factory->state(Todo::class, 'done', function (Faker $faker) {
    return [
        'done' => true,
    ];
});
