<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersCount = (int)$this->command->ask('Number of users?', 10);

        factory(User::class, $usersCount)->create();
        factory(User::class)->states('admin')->create();
    }
}
