<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if($this->command->confirm("Refresh database?", true)){
            $this->command->call('migrate:fresh');
            $this->command->info("Database refreshed");
        }

        $this->call([
            UserSeeder::class,
            TagSeeder::class,
            TodoListSeeder::class,
            TodoSeeder::class,
        ]);
    }
}
