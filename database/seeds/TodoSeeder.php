<?php

use App\Models\Tag;
use App\Models\Todo;
use App\Models\TodoList;
use Illuminate\Database\Seeder;

class TodoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $todoCount = (int)$this->command->ask('Number of Todos?', 200);
        $todoLists = TodoList::all();
        $tags = Tag::all();

        factory(Todo::class, $todoCount)->make()->each(function (Todo $todo) use ($todoLists, $tags) {
            $todo->todoList()->associate($todoLists->random())->save();
            $todo->tags()->attach($tags->random(rand(1, 3))->pluck('id')->toArray());
        });

        factory(Todo::class, $todoCount)->states('done')->make()->each(function (Todo $todo) use ($todoLists, $tags) {
            $todo->todoList()->associate($todoLists->random())->save();
            $todo->tags()->attach($tags->random(rand(1, 3))->pluck('id')->toArray());
        });

    }
}
