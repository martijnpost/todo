<?php

use App\Models\TodoList;
use App\Models\User;
use Illuminate\Database\Seeder;

class TodoListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $todoListCount = (int)$this->command->ask('Number of todoLists?', 30);
        $users = User::all();

        factory(TodoList::class, $todoListCount)->make()->each(function (TodoList $todoList) use ($users) {
            $todoList->user()->associate($users->random())->save();
        });
    }
}
