<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('todo_list_id')->index();
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('file')->nullable();
            $table->timestamp('deadline')->nullable();
            $table->boolean('done')->default(false);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign(['todo_list_id'])
                ->references('id')
                ->on('todo_lists')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todos');
    }
}
