<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::resource('todo-lists', 'TodoListsController');
Route::resource('todo-lists.todos', 'TodoController')->shallow();

Route::post('/todos/{todo}/done', 'TodoController@status')->name('status');
Route::post('/todos/{todo}/delay', 'TodoController@delay')->name('delay');

Route::resource('tags', 'TagController')->except('show');

Route::get('download', 'DownloadFileController')->name('download');
