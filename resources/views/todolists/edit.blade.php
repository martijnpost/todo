@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1>Edit Todo List: {{$todoList->title}}</h1>
        </div>
        <form action="{{ route('todo-lists.update', [$todoList]) }}" method="POST">
            @method("PATCH")
            @include('todolists._form')
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection
