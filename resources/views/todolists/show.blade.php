@extends('layouts.app')

@section('content')
    <div class="container">
       <h1>{{ $todoList->title  }}</h1>
        <p>{{ $todoList->description }}</p>
        <hr>
        <p class="text-muted">{{ $todoList->created_at->diffForHumans() }}</p>
    </div>
@endsection
