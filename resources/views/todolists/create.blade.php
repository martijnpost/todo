@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1>Create new Todo List</h1>
        </div>
        <form action="{{ route('todo-lists.store') }}" method="POST">
            @include('todolists._form')
            <input type="submit" value="Create" class="btn btn-primary">
        </form>
    </div>
@endsection
