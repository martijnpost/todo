@csrf
<div class="form-group">
    <label for="title">Title</label>
    <input type="text" name="title" class="form-control" id="title" placeholder="Title" value="{{ old('title') ?? $todoList->title ?? '' }}">
</div>
<div class="form-group">
    <label for="description">Description</label>
    <textarea name="description"
              id="description"
              rows="10"
              class="form-control"
              placeholder="Description">{{ old('description') ?? $todoList->description ?? '' }}
    </textarea>
</div>
