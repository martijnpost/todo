@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row d-flex justify-content-between mb-3">
            <h1>
                {{ $todoList->title }}
                @if($todoList->todos_count === $todoList->todos_done_count)
                    (Completed)
                @endif
            </h1>

            <form action="{{ route('todo-lists.todos.index', $todoList) }}" class="form-inline">
                <select class="form-control" name="tagFilter">
                    <option disabled selected>Select Tag</option>
                    <option value="">All</option>
                    @foreach($tags as $tag)
                        <option value="{{ $tag->id }}" {{ $tagFilter == $tag->id ? 'selected' : ''}}>{{ $tag->name }}</option>
                    @endforeach
                </select>
                <input type="submit" value="Filter" class="btn btn-outline-primary ml-2">
            </form>

            @can('todo.create', $todoList)
                <a href="{{ route('todo-lists.todos.create', $todoList) }}" class="btn btn-primary h-50">{{ __('Create Todo') }}</a>
            @endcan
        </div>
        <div class="row mb-4">
            <p class="text-black-50">{{ $todoList->description }}</p>
        </div>

        @if($status)
            <div class="progress mb-4">
                <div class="progress-bar"
                     role="progressbar"
                     style="width: {{ $status }}%"
                     aria-valuenow="{{ $status }}"
                     aria-valuemin="{{ $status }}"
                     aria-valuemax="100">
                    <span>{{ $status }}%</span>
                </div>

            </div>
        @endif

        <div class="table-responsive">
             @forelse($todos as $todo)
                 @if($loop->first)
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Title</th>
                                <th></th>
                                <th>Tags</th>
                                <th>Deadline</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                    @endif
                     <tr style="@if($todo->done) background-color: #e3f2fd; @endif
                                @if(!$todo->done && $todo->deadline && $todo->deadline <= now()) background-color: #ff6900 @endif">
                        <td>
                            @include('todos._delay')
                        </td>
                        <td>
                            {{ $todo->title }}
                        </td>
                        <td>
                            @if($todo->file)
                                <a href="{{ route('download', ['file' => $todo->file]) }}">file</a>
                            @endif
                        </td>
                         <td>
                             <h5>
                                @foreach($todo->tags as $tag)
                                    <span class="badge badge-info ">
                                        <a class="text-light"
                                           href="{{ route('todo-lists.todos.index', ['todo_list' => $todoList, 'tagFilter' => $tag]) }}">
                                            {{ $tag->name }}
                                        </a>
                                    </span>
                                 @endforeach
                             </h5>
                         </td>
                        <td>
                            <p data-toggle="{{ $todo->deadline <= now()->addDays(7) }}" data-placement="bottom" title="{{ Carbon\Carbon::parse($todo->deadline)->format('d-m-Y (h:m)') }}">
                                {{ $todo->deadline ? Carbon\Carbon::parse($todo->deadline)->diffForHumans() : "-" }}

                                @if($todo->deadline <= now()->addDays(2) &&  $todo->deadline && !$todo->done)
                                        <svg style="fill:red;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16"><path fill-rule="evenodd" d="M8.22 1.754a.25.25 0 00-.44 0L1.698 13.132a.25.25 0 00.22.368h12.164a.25.25 0 00.22-.368L8.22 1.754zm-1.763-.707c.659-1.234 2.427-1.234 3.086 0l6.082 11.378A1.75 1.75 0 0114.082 15H1.918a1.75 1.75 0 01-1.543-2.575L6.457 1.047zM9 11a1 1 0 11-2 0 1 1 0 012 0zm-.25-5.25a.75.75 0 00-1.5 0v2.5a.75.75 0 001.5 0v-2.5z"></path></svg>
                                @endif
                            </p>
                        </td>
                         <td>
                             <form id="status-form" action="{{ route('status', [$todo]) }}" method="POST">
                                 @csrf
                                 <input type="checkbox" @cannot('todo.update', $todo)disabled="disabled" @endcannot
                                        @if($todo->done) checked @endif onchange="this.form.submit()">
                             </form>
                         </td>

                         <td class="float-right">
                             @include('todos.buttons')
                         </td>
                     </tr>
                 @empty
                    <p>No todos yet! Click <a href="{{ route('todo-lists.todos.create', $todoList) }}">HERE</a> to create one!</p>
                 @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
