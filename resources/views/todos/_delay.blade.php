@can('todo.update', $todo)
    @if(!$todo->done && $todo->deadline)
        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#model{{ $todo->id }}">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path d="M12.5 7.25a.75.75 0 00-1.5 0v5.5c0 .27.144.518.378.651l3.5 2a.75.75 0 00.744-1.302L12.5 12.315V7.25z"></path><path fill-rule="evenodd" d="M12 1C5.925 1 1 5.925 1 12s4.925 11 11 11 11-4.925 11-11S18.075 1 12 1zM2.5 12a9.5 9.5 0 1119 0 9.5 9.5 0 01-19 0z"></path></svg>
        </button>

        <div class="modal fade" id="model{{ $todo->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delay '{{ $todo->title }}'</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('delay', $todo) }}" method="POST" id="delay-form">
                            @csrf
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label"></label>
                                Delay the todo with
                                <select name="delay">
                                    @foreach(range(1,14) as $day)
                                        <option value="{{ $day }}">{{ $day }}</option>
                                    @endforeach
                                </select>
                                days
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Oke</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endcan
