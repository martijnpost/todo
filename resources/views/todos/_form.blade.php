@csrf

<div class="form-group">
    <label for="title">Title</label>
    <input type="text"
           name="title"
           class="form-control @error('title') is-invalid @enderror"
           id="title"
           placeholder="Title"
            value="{{ old('title') ?? $todo->title ?? '' }}">
    @error('title')
        <div class="text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="description">Description</label>
    <textarea name="description"
              id="description"
              rows="10"
              class="form-control @error('description') is-invalid @enderror"
              placeholder="Description"
    >{{ old('description') ?? $todo->description ?? '' }}</textarea>
    @error('description')
        <div class="text-danger">{{ $message }}</div>
    @enderror
</div>


<div class="form-group">
    <label class="mdb-main-label d-flex" for="tags">Tags (max 3)</label>
    <select name="tags[]" class="form-control md-form selectpicker @error('tags') is-invalid @enderror" multiple>
        @foreach($tags as $tag)
            <option value="{{ $tag->id }}" @if(isset($todo)){{ $todo->tags()->find($tag->id) ? 'selected' : '' }} @endif>{{ $tag->name }}</option>
        @endforeach
    </select>
    @error('tags')
        <div class="text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="url">Url</label>
    <input type="text"
           name="url"
           id="url"
           class="form-control @error('url') is-invalid @enderror"
           value="{{ old('url') ?? $todo->url ?? '' }}">
    @error('url')
    <div class="text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="file">File</label>
    <input type="file"
           name="file"
           id="file"
           class="form-control-file @error('file') is-invalid @enderror">
    @error('file')
        <div class="text-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="deadline">Deadline</label>
    <input type="datetime-local"
           id="deadline"
           name="deadline"
           value="{{ old('deadline') ?? (isset($todo) ? Carbon\Carbon::parse($todo->deadline)->toDateTimeLocalString() : '') }}"
           class="d-block form-control @error('deadline') is-invalid @enderror">
    @error('deadline')
        <div class="text-danger">{{ $message }}</div>
    @enderror
</div>
