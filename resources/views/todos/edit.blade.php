@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1>Edit todo '{{ $todo->title }}'</h1>
        </div>
        <div class="row">
            <div class="col-9">
                <form method="POST" action="{{ route('todos.update', $todo) }}" enctype="multipart/form-data">
                    @method('PATCH')
                    @include('todos._form')

                    <div class="form-group">
                        <input type="submit" value="Edit" class="btn btn-primary">
                    </div>
                </form>
            </div>

            {{-- create new Tag--}}
            <div class="col-3">
                <div class="form-group border-left pl-2">
                    @include('tags.create')
                </div>
            </div>
        </div>
    </div>
@endsection
