@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1>
                <span class="mr-3">
                    {{ $todo->title }}
                </span>
                @foreach($todo->tags as $tag)
                    <span class="badge badge-info">
                       {{ $tag->name }}
                    </span>
                @endforeach
            </h1>
        </div>

        <div class="row">
            <p class="text-muted">
                {{ $todo->description }}
            </p>
        </div>
        <hr>
        <div class="row">
            @if($todo->deadline)
                <p>Deadline: {{ $todo->deadline }}  ({{ Carbon\Carbon::parse($todo->deadline)->diffForHumans() }})</p>
            @endif
        </div>

        <div class="row">
            @if($todo->url)
                <p>
                    Url: <a href="{{ $todo->url }}" target="_blank">{{ $todo->url }}</a>
                </p>
            @endif
        </div>

        <div class="row">
            @if($todo->file)
                <p>
                    File: <a href="{{ route('download', ['file' => $todo->file]) }}"> Download</a>
                </p>
            @endif
        </div>

        <div class="row">
            <a href="{{ url()->previous() }}" class="btn btn-primary">Go Back</a>
        </div>
    </div>
@endsection
