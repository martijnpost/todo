@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1>Create new todo</h1>
        </div>
        <div class="row">
            <div class="col-9">
                <form method="POST" action="{{ route('todo-lists.todos.store', $todoList) }}" enctype="multipart/form-data">

                   @include('todos._form')

                    <div class="form-group">
                        <input type="submit" value="Create" class="btn btn-primary">
                        <a href="{{ url()->previous() }}" class="btn-link ml-2">Back</a>
                    </div>
                </form>
            </div>

            {{-- create new Tag--}}
            <div class="col-3">
                <div class="form-group border-left pl-2">
                    @include('tags.create')
                </div>
            </div>
        </div>
    </div>
@endsection
