@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1>Tags</h1>
            </div>
            <div class="col-2">
                <a href="#" class="btn btn btn-primary">Create new tag</a>
            </div>
            <table class="table mt-3">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Used</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($tags as $tag)
                        <tr>
                            <td>{{ $tag->name }}</td>
                            <td data-toggle="tooltip" data-placement="right" title="@foreach($tag->todos as $todo) {{ '-'.$todo->title."\n" }} @endforeach">
                                {{ $tag->todos_count }}

                            </td>
                            <td class="float-right">
                                @include('tags.buttons')
                            </td>
                        </tr>
                    @empty
                        <p>No tags yet!</p>
                    @endforelse
                </tbody>
            </table>
            {{ $tags->links() }}
        </div>
    </div>

@endsection
