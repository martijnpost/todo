<form id="createTagForm" action="{{ route('tags.store') }}" method="POST">
    @csrf
    <label for="name">Create new tag</label>
    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror">
    @error('name')
        <div class="text-danger">{{ $message }}</div>
    @enderror
    <input type="submit" class="btn btn-info form-control mt-3" value="Create" form="createTagForm">
</form>
