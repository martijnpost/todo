@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row d-flex justify-content-between mb-3">
        <h1>Todo Lists</h1>
        @guest()
            <a href="{{ route('login') }}" class="btn btn-primary h-50">{{ __('Login') }}</a>
        @else
            <a href="{{ route('todo-lists.create') }}" class="btn btn-primary h-50">Create Todo List</a>
        @endguest
    </div>
    @forelse($users as $user)
        @if($loop->first)
            <div class="row">
                <div class="col-3">
                    <h3>User</h3>
                </div>
                <div class="col-6">
                    <h3>Todo Lists</h3>
                </div>
                <div class="col-3">
                    <h3>Todos</h3>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-3">
                {{ $user->name }}
            </div>
            <div class="col-6">
                <ul>
                    @forelse($user->todoLists as $todoList)
                        <li>
                            <a href="{{ route('todo-lists.show', $todoList) }}">{{ $todoList->title }}</a>
                        </li>
                    @empty
                        <p class="text-muted">No todo lists!</p>
                    @endforelse
                </ul>
            </div>
            <div class="col-3">
                @forelse($user->todoLists as $todoList)
                    {{ $todoList->todos_count }}<br>
                @empty
                    <p class="text-muted">No todos!</p>
                @endforelse
            </div>
        </div>
    @empty
        <p class="text-muted">No users yet! Click <a href="{{ route('register') }}">HERE</a> to register!</p>
    @endforelse
    {{ $users->links() }}
</div>
@endsection
